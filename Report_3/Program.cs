﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report_3
{


    class Program
    {
        static void Main()
        {
            string reGame;

            while (true)
            {
                Player player1 = new Player("Player 1");
                player1.GetCard();
                player1.GetResut();


                Console.WriteLine();

                Player player2 = new Player("Player 2");
                player2.GetCard();
                player2.GetResut();

                if(player1.GetResultType > player2.GetResultType)
                {
                    Console.WriteLine(player1.PlayerName + "Win");
                }
                else if (player1.GetResultType < player2.GetResultType)
                {
                    Console.WriteLine(player2.PlayerName + "Win");
                }
                else
                {
                    if (player1.GetScore > player2.GetScore)
                    {
                        Console.WriteLine(player1.PlayerName + "Win");
                    }
                    else if (player2.GetScore > player1.GetScore)
                    {
                        Console.WriteLine(player2.PlayerName + "Win");
                    }
                    else
                    {
                        Console.WriteLine("Draw");
                    }
                }


                do
                {
                    Console.Write("\n\n한번 더 하시겠습니까? (y/n) : ");
                    reGame = Console.ReadLine().ToLower();
                }
                while (reGame != "y" && reGame != "n");

                if (reGame == "n") return;
                Console.WriteLine();
            }
        }       
     
    }


    class Player
    {
        Random rand = new Random();
        Card[] cards = new Card[7];

        int resultType = -1;
        int maxValue = 1;

        string[] names = { "2", "3", "4", "5", "6", "7", "8", "9", "T(10)", "J", "Q", "K", "A" };
        string[] suits = { "Spade", "Heart", "Diamond", "Club" };

        public Player(string name)
        {
            PlayerName = name;
        }

        public string PlayerName
        {
            get; set;
        }

        public int GetResultType
        {
            get { return resultType; }
        }

        public int GetScore
        {
            get { return resultType * maxValue; }
        }

        public void GetCard()
        {
                     
            for(int i = 0; i<7; i++)
            {                             
                cards[i] = new Card(rand.Next(13), rand.Next(4));              
         
            }

            Console.Write(PlayerName + " : ");

            foreach (Card card in cards)
            {
                Console.Write("{0} ", card);
            }

            Console.WriteLine();
        }

       
        public void GetResut()
        {
            string result = string.Empty;

            //같은 숫자끼리 그룹을 만든다.
            var groups = from card in cards
                            group card by card.Value into g
                            orderby g.Count() descending, g.Key
                            select g;            

            
            int firstGroupCount = groups.First().Count();       //첫번째 그룹의 카운터
            int secondGroupCount = groups.ElementAt(1).Count();
            int firstGroupKey = groups.First().Key;
            int secondGroupKey = groups.ElementAt(1).Key;

            if (firstGroupCount == 4)
            {
                resultType = 8;
                result = "Four of a Kind " + names[firstGroupKey];              
            }
            else if (firstGroupCount == 3 && secondGroupCount >= 2)
            {
                resultType = 7;
                result = "Full House - " + names[firstGroupKey] + " and " + names[secondGroupKey];               
            }
            else if (firstGroupCount == 3)
            {
                resultType = 4;
                result = "Three of a Kind " + names[firstGroupKey];              
            }
            else if (firstGroupCount == 2 && secondGroupCount == 2)
            {
                resultType = 3;
                result = "Two Pairs - " + names[firstGroupKey] + " and " + names[secondGroupKey];
            }
            else if (firstGroupCount == 2)
            {
                resultType = 2;
                result = "One Pair " + names[firstGroupKey];
            }
            else
            {
                resultType = 1;
                result = names[firstGroupKey] + " Card";
            }

            //첫번 째 그룹의 최고 값을 저장한다.
            maxValue = firstGroupKey;

          
            cards = (from card in cards orderby card.Value select card).ToArray(); //오름 차순으로 카드를 정렬한다.


            Card[] distinctCards = cards.Distinct(new CardValueComparer()).ToArray(); // 카드 값을 비교하여 중첩되는 카드를 배제한다.

            bool straight = false;

            if (distinctCards.Length >= 5) //카드 정리 후, 5장 이상일 때, 스트레이트 인지를 확인한다.
            {
                for (int i = 0; i < distinctCards.Length - 4; i++)
                {
                    if (distinctCards[i].Value == distinctCards[i + 4].Value - 4)
                    {
                        if (resultType < 5)
                        {
                            result = names[distinctCards[i].Value + 4] + " Straight";
                            resultType = 5;
                            maxValue = distinctCards[i].Value + 4;
                        }
                        straight = true;
                        break;
                    }
                }
            }


            //카드 무늬로 그룹을 만들고 정렬한다.
            groups = from card in cards
                        group card by card.Suit into g
                        orderby g.Count() descending
                        select g;


            //동일한 무늬가 5개 이상이면 Flush 이다.
            int count = groups.First().Count();

            if (count >= 5)
            {
                int index = groups.First().First().Value;
                int suit = groups.First().Key;

                if (resultType < 6)
                {
                    resultType = 6;
                    result = names[index] + " Flush in " + suits[suit];
                    maxValue = index;
                }

                if (straight)
                {
                    // 이전에 카드의 값들이 스트레이트였다면, 동일한 무늬의 그룹에서 다시 한번 스트레이트인지를 검사해서 Straight Flush 인지를 확인한다.
                    Card[] flushCards = (from card in groups.First() orderby card.Value select card).ToArray();

                    for (int i = 0; i < count - 4; i++)
                    {
                        if (flushCards[i].Value == flushCards[i + 4].Value - 4)
                        {
                            bool straightFlush = true;
                            int flushSuit = flushCards[i].Suit;

                            for (int j = i + 1; j < i + 5; j++)
                            {
                                if (flushSuit != flushCards[j].Suit)
                                {
                                    straightFlush = false;
                                    break;
                                }
                            }

                            if (straightFlush)
                            {
                                resultType = 9;
                                result = names[flushCards[i].Value + 4] + " Straight Flush " + suits[flushCards[i].Suit];
                                maxValue = flushCards[i].Value + 4;

                                if(flushCards[i].Value == 8)
                                {
                                    resultType = 9;
                                    result = "Royal Flush in" + suits[flushCards[i].Suit];
                                }


                                break;
                            }
                        }
                    }
                }
            }

     
            Console.WriteLine(PlayerName + " : " + result);
        }
    }

    class Card
    {      
        static string values = "23456789TJQKA";
        static string suits = "shdc";

        public int Value;
        public int Suit;

        public Card(int value, int suit)
        {
            Value = value;
            Suit = suit;
        }

        public override string ToString()
        {
            return String.Format("{0}{1}", values[Value], suits[Suit]);
        }


    }

    class CardValueComparer : IEqualityComparer<Card>
    {
        public bool Equals(Card c1, Card c2)
        {
            return c1.Value == c2.Value;
        }

        public int GetHashCode(Card c)
        {
            return c.Value.GetHashCode();
        }
    }
}
