﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report_1
{
    class Program
    {
        static void Main(string[] args)
        {
                      
            List<string> likes = new List<string>();
            GetLikes(likes);
            

            likes.Add("Rafy");
            GetLikes(likes);

            likes.Add("Ryan");
            GetLikes(likes);

            likes.Add("Curian");
            GetLikes(likes);

            likes.Add("Billy");
            GetLikes(likes);

            likes.Add("Green");
            GetLikes(likes);

            Console.ReadLine();
        }

        private static void GetLikes(List<String> value)
        {
            
            if(value == null)
            {
                return;
            }

            switch(value.Count)
            {

                case 0 :
                     Console.WriteLine("no one likes this");                
                     break;

                case 1:
                     Console.WriteLine(string.Format("{0} like this", value[0]));
                    break;

                case 2:
                     Console.WriteLine(string.Format("{0} and {1} like this", value[0], value[1]));
                     break;

                case 3:
                    Console.WriteLine(string.Format("{0}, {1} and {2} like this", value[0], value[1], value[2]));
                    break;

                default:

                    Console.WriteLine(string.Format("{0}, {1} and {2} others like this", value[0], value[1], value.Count - 2));

                    break;
            }   

        }
    }
}
