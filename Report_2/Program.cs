﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report_2
{
    class Program
    {
        enum Priority
        {
            Tag,
            Paragraph,
            None
        }

        static void Main(string[] args)
        {
            Reverse("The quick <font color=\"brown\">brown</font> fox jumps over the lazy dog");
        }

        static void Reverse(String str)
        {          
            Console.WriteLine("입력값은 \"{0}\" 입니다.", str);

            Priority priority = Priority.None; //입력된 문자열의 시작 순서를 구분하는 플래그

            List<string> paragrapgList = new List<string>(); //HTML 태그가 아닌 문자열을 문단 단위로 저장하는 리스트
            List<string> htmlTagList = new List<string>();  //HTML 태그를 저장하는 리스트

            bool isTag = false; //HTM 태그를 구분하기 위한 플래그
            StringBuilder sb = new StringBuilder();
            
            for(int i = 0; i < str.Length; i++)
            {
                if(str[i].ToString() == "<")
                {   
                    //HTML Tag 가 시작되면 이전에 저장된 문자들을 하나의 문단으로 저장한다.

                    isTag = true;

                    if(sb.Length > 0)
                    {
                        //문장 리스트에 추가하고, 저장된 문자열을 지운다.
                        paragrapgList.Add(sb.ToString());
                        sb.Clear();

                        //시작의 우선 순위가 저장되지 않았다면 문자열로 지정한다.
                        if(priority == Priority.None)
                        {
                            priority = Priority.Paragraph;
                        }
                    }
                }

                sb.Append(str[i]);

                if (isTag)
                {                   
                    if (str[i].ToString() == ">")
                    {
                        //HTML Tag 가 끝나면 저장된 HTML Tag 문자를 하나의 Tag 저장한다.

                        isTag = false;                       

                        if (sb.Length > 0)
                        { 
                            //태그 리스트에 추가하고, 저장된 문자열을 지운다.
                            htmlTagList.Add(sb.ToString());
                            sb.Clear();

                            //시작의 우선 순위가 저장되지 않았다면 HTML Tag로 지정한다.
                            if (priority == Priority.None)
                            {
                                priority = Priority.Tag;
                            }
                        }
                    }
                }
               
            }


            if (sb.Length > 0)
            {
                //마지막에 저장된 문자가 있다면, 문장 리스트에 추가한다.
                paragrapgList.Add(sb.ToString());
                sb.Clear();
            }

            
            

            if (priority == Priority.Paragraph) //문장으로 시작할 때
            {
                int currentIndex = 0;

                for (int i = paragrapgList.Count; i > 0; i--)
                {
                    for (int j = paragrapgList[i - 1].Length; j > 0; j--)
                    {
                        sb.Append(paragrapgList[i - 1].ToString()[j - 1]);
                    }

                    if (htmlTagList.Count > 0 && currentIndex < htmlTagList.Count)
                    {
                        sb.Append(htmlTagList[currentIndex].ToString());
                        currentIndex++;
                    }
                }
            }
            else //태그로 시작할 때
            {
                int currentIndex = paragrapgList.Count;

                for (int i = 0; i < htmlTagList.Count; i++)
                {
                    if (htmlTagList.Count > 0)
                    {
                        sb.Append(htmlTagList[i].ToString());                       
                    }

                    for (int j = paragrapgList[currentIndex - 1].Length; j > 0; j--)
                    {
                        sb.Append(paragrapgList[currentIndex - 1].ToString()[j - 1]);
                    }

                }
            }

          
            Console.WriteLine("\n\n");
            Console.WriteLine("출력값은 \"{0}\" 입니다.", sb.ToString());


            Console.ReadKey();



        }

    }
}
